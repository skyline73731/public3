$('.slider').slick({
    autoplay: false,
    autoplaySpeed: 2000,
    dots: true,
    arrows: false
  });

$(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() != 0) {
			$('#toTop').fadeIn();
		} else {
			$('#toTop').fadeOut();
		}
	 
	});
	$('#toTop').click(function() {
		$('body,html').animate({scrollTop:0},800);
	});
});